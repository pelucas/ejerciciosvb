﻿Public Class Soles
    Private Sub BtnCalcular_Click(sender As Object, e As EventArgs) Handles btnCalcular.Click
        Dim DOLAR, EURO As Decimal

        DOLAR = nudSoles.Value / 3.36
        EURO = nudSoles.Value / 3.74

        txtDolar.Text = Math.Round(DOLAR, 2)
        txtEuro.Text = Math.Round(EURO, 2)

    End Sub

End Class