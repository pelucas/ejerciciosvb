﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Trapecio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCalcular = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPerimetro = New System.Windows.Forms.TextBox()
        Me.nudLadoA = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.nudLadoB = New System.Windows.Forms.NumericUpDown()
        Me.nudLadoC = New System.Windows.Forms.NumericUpDown()
        Me.nudLadoD = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtArea = New System.Windows.Forms.TextBox()
        Me.nudAltura = New System.Windows.Forms.NumericUpDown()
        Me.Label7 = New System.Windows.Forms.Label()
        CType(Me.nudLadoA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLadoB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLadoC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLadoD, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudAltura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCalcular
        '
        Me.btnCalcular.Location = New System.Drawing.Point(275, 60)
        Me.btnCalcular.Name = "btnCalcular"
        Me.btnCalcular.Size = New System.Drawing.Size(102, 55)
        Me.btnCalcular.TabIndex = 0
        Me.btnCalcular.Text = "Calcular"
        Me.btnCalcular.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(64, 46)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Lado A"
        '
        'txtPerimetro
        '
        Me.txtPerimetro.Enabled = False
        Me.txtPerimetro.Location = New System.Drawing.Point(485, 46)
        Me.txtPerimetro.Name = "txtPerimetro"
        Me.txtPerimetro.Size = New System.Drawing.Size(128, 20)
        Me.txtPerimetro.TabIndex = 2
        '
        'nudLadoA
        '
        Me.nudLadoA.Location = New System.Drawing.Point(111, 44)
        Me.nudLadoA.Name = "nudLadoA"
        Me.nudLadoA.Size = New System.Drawing.Size(120, 20)
        Me.nudLadoA.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(64, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Lado B"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(64, 102)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Lado C"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(63, 130)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(42, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Lado D"
        '
        'nudLadoB
        '
        Me.nudLadoB.Location = New System.Drawing.Point(111, 72)
        Me.nudLadoB.Name = "nudLadoB"
        Me.nudLadoB.Size = New System.Drawing.Size(120, 20)
        Me.nudLadoB.TabIndex = 7
        '
        'nudLadoC
        '
        Me.nudLadoC.Location = New System.Drawing.Point(111, 100)
        Me.nudLadoC.Name = "nudLadoC"
        Me.nudLadoC.Size = New System.Drawing.Size(120, 20)
        Me.nudLadoC.TabIndex = 8
        '
        'nudLadoD
        '
        Me.nudLadoD.Location = New System.Drawing.Point(111, 128)
        Me.nudLadoD.Name = "nudLadoD"
        Me.nudLadoD.Size = New System.Drawing.Size(120, 20)
        Me.nudLadoD.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(426, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Perímetro"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(450, 84)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Área"
        '
        'txtArea
        '
        Me.txtArea.Enabled = False
        Me.txtArea.Location = New System.Drawing.Point(485, 84)
        Me.txtArea.Name = "txtArea"
        Me.txtArea.Size = New System.Drawing.Size(128, 20)
        Me.txtArea.TabIndex = 12
        '
        'nudAltura
        '
        Me.nudAltura.Location = New System.Drawing.Point(111, 154)
        Me.nudAltura.Name = "nudAltura"
        Me.nudAltura.Size = New System.Drawing.Size(120, 20)
        Me.nudAltura.TabIndex = 14
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(71, 156)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Altura"
        '
        'Trapecio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(770, 209)
        Me.Controls.Add(Me.nudAltura)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtArea)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.nudLadoD)
        Me.Controls.Add(Me.nudLadoC)
        Me.Controls.Add(Me.nudLadoB)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.nudLadoA)
        Me.Controls.Add(Me.txtPerimetro)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnCalcular)
        Me.Name = "Trapecio"
        Me.Text = "Trapecio"
        CType(Me.nudLadoA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLadoB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLadoC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLadoD, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudAltura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnCalcular As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtPerimetro As TextBox
    Friend WithEvents nudLadoA As NumericUpDown
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents nudLadoB As NumericUpDown
    Friend WithEvents nudLadoC As NumericUpDown
    Friend WithEvents nudLadoD As NumericUpDown
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txtArea As TextBox
    Friend WithEvents nudAltura As NumericUpDown
    Friend WithEvents Label7 As Label
End Class
