﻿Public Class MB
    Private Sub BtnCalcular_Click(sender As Object, e As EventArgs) Handles btnCalcular.Click
        Dim MB As UShort
        Dim BTE, KB, GB As Decimal

        MB = nudMB.Value

        GB = MB / 1024
        KB = MB * 1024
        BTE = KB * 1024

        txtByte.Text = BTE
        txtKbyte.Text = KB
        txtGB.Text = GB
        btnLimpiar.Enabled = True
        btnCalcular.Enabled = False
        nudMB.Enabled = False

    End Sub

    Private Sub BtnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click

        txtByte.Text = ""
        txtKbyte.Text = ""
        txtGB.Text = ""
        nudMB.Value = 1
        btnLimpiar.Enabled = False
        btnCalcular.Enabled = True
        nudMB.Enabled = True

    End Sub
End Class