﻿Public Class Tiempo_Horas
    Private Sub BtnCalcular_Click(sender As Object, e As EventArgs) Handles btnCalcular.Click
        Dim Hora As UShort
        Dim Min, Seg, MSeg, Dia, Sem As Decimal

        Hora = nudHora.Value

        Min = Hora * 60
        Seg = Min * 60
        MSeg = Seg * 1000
        Dia = Hora / 24
        Sem = Hora / 168

        txtDia.Text = Dia
        txtMinuto.Text = Min
        txtMSegundo.Text = MSeg
        txtSegundo.Text = Seg
        txtSemana.Text = Sem

    End Sub
End Class