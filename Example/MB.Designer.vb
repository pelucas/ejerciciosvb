﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MB
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCalcular = New System.Windows.Forms.Button()
        Me.lblMB = New System.Windows.Forms.Label()
        Me.lblGB = New System.Windows.Forms.Label()
        Me.lblKbyte = New System.Windows.Forms.Label()
        Me.txtKbyte = New System.Windows.Forms.TextBox()
        Me.txtGB = New System.Windows.Forms.TextBox()
        Me.nudMB = New System.Windows.Forms.NumericUpDown()
        Me.txtByte = New System.Windows.Forms.TextBox()
        Me.btnLimpiar = New System.Windows.Forms.Button()
        Me.lblByte = New System.Windows.Forms.Label()
        CType(Me.nudMB, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCalcular
        '
        Me.btnCalcular.Location = New System.Drawing.Point(74, 81)
        Me.btnCalcular.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCalcular.Name = "btnCalcular"
        Me.btnCalcular.Size = New System.Drawing.Size(118, 27)
        Me.btnCalcular.TabIndex = 0
        Me.btnCalcular.Text = "Calcular"
        Me.btnCalcular.UseVisualStyleBackColor = True
        '
        'lblMB
        '
        Me.lblMB.AutoSize = True
        Me.lblMB.Location = New System.Drawing.Point(13, 41)
        Me.lblMB.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMB.Name = "lblMB"
        Me.lblMB.Size = New System.Drawing.Size(171, 17)
        Me.lblMB.TabIndex = 1
        Me.lblMB.Text = "Ingrese un número en MB"
        '
        'lblGB
        '
        Me.lblGB.AutoSize = True
        Me.lblGB.Location = New System.Drawing.Point(39, 184)
        Me.lblGB.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblGB.Name = "lblGB"
        Me.lblGB.Size = New System.Drawing.Size(28, 17)
        Me.lblGB.TabIndex = 4
        Me.lblGB.Text = "GB"
        '
        'lblKbyte
        '
        Me.lblKbyte.AutoSize = True
        Me.lblKbyte.Location = New System.Drawing.Point(23, 154)
        Me.lblKbyte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblKbyte.Name = "lblKbyte"
        Me.lblKbyte.Size = New System.Drawing.Size(44, 17)
        Me.lblKbyte.TabIndex = 5
        Me.lblKbyte.Text = "Kbyte"
        '
        'txtKbyte
        '
        Me.txtKbyte.Enabled = False
        Me.txtKbyte.Location = New System.Drawing.Point(74, 158)
        Me.txtKbyte.Name = "txtKbyte"
        Me.txtKbyte.Size = New System.Drawing.Size(165, 23)
        Me.txtKbyte.TabIndex = 7
        '
        'txtGB
        '
        Me.txtGB.Enabled = False
        Me.txtGB.Location = New System.Drawing.Point(74, 187)
        Me.txtGB.Name = "txtGB"
        Me.txtGB.Size = New System.Drawing.Size(165, 23)
        Me.txtGB.TabIndex = 8
        '
        'nudMB
        '
        Me.nudMB.Location = New System.Drawing.Point(191, 39)
        Me.nudMB.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudMB.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMB.Name = "nudMB"
        Me.nudMB.Size = New System.Drawing.Size(120, 23)
        Me.nudMB.TabIndex = 9
        Me.nudMB.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtByte
        '
        Me.txtByte.Enabled = False
        Me.txtByte.Location = New System.Drawing.Point(74, 127)
        Me.txtByte.Name = "txtByte"
        Me.txtByte.Size = New System.Drawing.Size(165, 23)
        Me.txtByte.TabIndex = 10
        '
        'btnLimpiar
        '
        Me.btnLimpiar.Enabled = False
        Me.btnLimpiar.Location = New System.Drawing.Point(219, 81)
        Me.btnLimpiar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(118, 27)
        Me.btnLimpiar.TabIndex = 11
        Me.btnLimpiar.Text = "Limpiar"
        Me.btnLimpiar.UseVisualStyleBackColor = True
        '
        'lblByte
        '
        Me.lblByte.AutoSize = True
        Me.lblByte.Location = New System.Drawing.Point(23, 130)
        Me.lblByte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblByte.Name = "lblByte"
        Me.lblByte.Size = New System.Drawing.Size(36, 17)
        Me.lblByte.TabIndex = 12
        Me.lblByte.Text = "Byte"
        '
        'MB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(508, 260)
        Me.Controls.Add(Me.lblByte)
        Me.Controls.Add(Me.btnLimpiar)
        Me.Controls.Add(Me.txtByte)
        Me.Controls.Add(Me.nudMB)
        Me.Controls.Add(Me.txtGB)
        Me.Controls.Add(Me.txtKbyte)
        Me.Controls.Add(Me.lblKbyte)
        Me.Controls.Add(Me.lblGB)
        Me.Controls.Add(Me.lblMB)
        Me.Controls.Add(Me.btnCalcular)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "MB"
        Me.Text = "MB"
        CType(Me.nudMB, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnCalcular As Button
    Friend WithEvents lblMB As Label
    Friend WithEvents lblGB As Label
    Friend WithEvents lblKbyte As Label
    Friend WithEvents txtKbyte As TextBox
    Friend WithEvents txtGB As TextBox
    Friend WithEvents nudMB As NumericUpDown
    Friend WithEvents txtByte As TextBox
    Friend WithEvents btnLimpiar As Button
    Friend WithEvents lblByte As Label
End Class
