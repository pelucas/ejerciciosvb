﻿Public Class Trapecio
    Private Sub BtnCalcular_Click(sender As Object, e As EventArgs) Handles btnCalcular.Click
        Dim A, B, C, D, H, Perimetro As Integer
        Dim Area As Decimal

        A = nudLadoA.Value
        B = nudLadoB.Value
        C = nudLadoC.Value
        D = nudLadoD.Value
        H = nudAltura.Value

        Perimetro = A + B + C + D

        Area = ((A + B) / 2) * H

        txtPerimetro.Text = Perimetro

        txtArea.Text = Area
    End Sub
End Class